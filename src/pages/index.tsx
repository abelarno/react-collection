import Image from "next/image";
import ReactDOM from 'react-dom'
import { Html, Head, Main, NextScript } from "next/document";
import { Inter } from "next/font/google";
import { Todo } from "../components/Todo";
import { Memory } from "../components/Memory";
import { Exchange } from "../components/Exchange";
import { Tracker } from "../components/Tracker";
import { GleamyProvider } from "gleamy";


const inter = Inter({ subsets: ["latin"] });

export default function Home() {



  function loadApp(e) {
    e.preventDefault();
    const appSelect = document.querySelector('#app-select')
    console.log(appSelect.value)

  }



  return (
    <>
      <GleamyProvider>
        <main>
          <select name="apps" id="app-select">
            <option value="">--Please choose an option--</option>
            <option value="tracker">Tracker</option>
            <option value="exchange">Exchange</option>
            <option value="memory">Memory</option>
            <option value="todo">Todo</option>
          </select>
          <button id="btn" onClick={loadApp}>Load app</button>


          <div id="root">
          <Tracker />
          <hr />
          <Exchange />
          <hr />
          <Memory />
          <hr />
          <Todo />
          </div>
        </main>
      </GleamyProvider>
    </>
  );
}
