import { useState, useEffect } from "react";
import { GleamyProvider, Gold, Steel, Gunmetal } from 'gleamy'


export const Todo = () => {

  const [list, setList] = useState([]);
  const [removeItemIndex, setRemoveItemIndex] = useState(undefined);

  useEffect(() => {
    const savedlist = JSON.parse(localStorage.getItem("list"));
    console.log("loaded", savedlist);
    setList(savedlist);
  }, []);

  useEffect(() => {
    if (list.length > 0) {
      save();
    }
  }, [list]);

  const toggle = (todoindex) => {
    return () => {
      const newList = list.map((item, index) => {
        if (todoindex === index) {
          return { ...item, done: !item.done };
        }
        return item;
      });
      setList(newList);
    };
  };

  const onchange = (todoindex) => (event) => {
    const newList = list.map((item, index) => {
      if (todoindex === index) {
        return { ...item, text: event.target.value };
      }
      return item;
    });
    setList(newList);
  };

  const save = () => {
    localStorage.setItem("list", JSON.stringify(list));
  };

  const renderList = list.map((item, index) => (
    <div className="listitem" key={index}>
      <input
        type="checkbox"
        className="checkbox"
        checked={!!item.done}
        onChange={toggle(index)}
      />

      {!item.done ? (
        <>
          <input
            type="text"
            value={item.text}
            onChange={onchange(index)}
            className="todoinput"
            onBlur={() => {
              !item.text && remove(index);
            }}
          />
        </>
      ) : (
        <>
          <div className="itemdone">{item.text}</div>
        </>
      )}
      {!item.done ? (
        <>
          <button
            className="deletebutton"
            onClick={() => setRemoveItemIndex(index)}
          >
            X
          </button>
        </>
      ) : (
        <>
          <button className="deletebutton" onClick={() => remove(index)}>
            X
          </button>
        </>
      )}
    </div>
  ));

  function add() {
    const newList = [...list];
    const todofield = document.getElementById("todofield").value;
    if (todofield === "") {
      console.log("mag niet leeg");
    } else {
      const todo = {
        text: todofield,
        done: false,
      };
      setList((newList) => [...newList, todo]);
    }
    if (typeof document === "object") {
      document.getElementById("todofield").value = "";
    }
    event.preventDefault();
  }

  function remove(index) {
    const newList = [...list];
    newList.splice(index, 1);
    setList(newList);
  }


  


  return (
    <>
      <h1>Abels bovenste beste TODO list</h1>
      <form>
        <label>
          Wat moet er gedaan worden? <br />
          
          <input type="text" id="todofield" className="inputfield" />
        </label>
    
          <button type="submit" className="addbutton" onClick={add}>
<Gunmetal edgeThickness={5} style={{ position: 'absolute', zIndex: -10, width: '500px', height: '50px', pointerEvents: 'none', padding: '0px', marginTop: '-16px', marginLeft: '-6px'}} />
             Voeg toe
          </button>
        </form>
      

      <h2>Wat moet er nog gebeuren?</h2>
      
      <div className="renderlist">{renderList}</div>

      {typeof removeItemIndex === "number" && (
        <div className="popup">
          <h3>Weet je zeker dat je het item wilt verwijderen</h3>
          <button
            type="button"
            onClick={() => {
              remove(removeItemIndex);
              setRemoveItemIndex(undefined);
            }}
          >
            Verwijder
          </button>
          <button type="button" onClick={() => setRemoveItemIndex(undefined)}>
            cancel
          </button>
        </div>
      )}
    </>
  );
};
