import { useState, useEffect, useRef, useMemo } from "react";

export const Tracker = () => {
  const [table, setTable] = useState([
    [
      { name: "high", active: false },
      { name: "mid", active: false },
      { name: "low", active: false },
      { name: "kick", active: false },
    ],
    [
      { name: "high", active: false },
      { name: "mid", active: false },
      { name: "low", active: false },
      { name: "kick", active: false },
    ],
    [
      { name: "high", active: false },
      { name: "mid", active: false },
      { name: "low", active: false },
      { name: "kick", active: false },
    ],
    [
      { name: "high", active: false },
      { name: "mid", active: false },
      { name: "low", active: false },
      { name: "kick", active: false },
    ],
  ]);

  const blockRefs = useRef([]);
  const columnRefs = useRef([]);

  const [barsAmount, setBarsamount] = useState(6);
    const [bpm, setBpm] = useState(180);
  const intervalTime = (60 / bpm) * 1000;



  function barsMultiplier() {
    var newBarsAmount = [...table[0]];
    var newnewBarsAmount = [];
    for (var i = 0; i < barsAmount; i++) {
      newnewBarsAmount.push(newBarsAmount);
    }
    console.log(newnewBarsAmount);
    setTable(newnewBarsAmount);
  }

  useEffect(() => {
    var i = 0;

    const interval = setInterval(() => {
      if (i == table.length) {
        const columnNotActive = columnRefs.current[i - 1];
        columnNotActive.className = "column passive";
        i = 0;
      }
      if (!(i == 0)) {
        const columnNotActive = columnRefs.current[i - 1];
        columnNotActive.className = "column passive";
      }

      const columnActive = columnRefs.current[i];
      columnActive.className = "column active";

      playSounds(i);
      i++;
    }, intervalTime);

    return () => clearInterval(interval);
  }, [intervalTime, table]);

  function playSounds(i) {
  
    var snare = new Audio("/snare.mp3");
    var bass = new Audio("/bass.mp3");
    var mid = new Audio("/mid.mp3");
    var high = new Audio("/high.mp3");
    const result = table[i].filter((item) => item.active === true);

    for (var i = 0; i < result.length; i++) {
      if (result[i].name == "kick") {
        snare.play();
      }
      if (result[i].name == "high") {
        high.play();
      }
      if (result[i].name == "mid") {
        mid.play();
      }
      if (result[i].name == "low") {
        bass.play();
      }
    }
  }

  const toggle = (itemName, rowindex, columnIndex) => {
    const newTable = [...table];

    newTable[columnIndex][rowindex].active
      ? (newTable[columnIndex][rowindex] = {
          name: itemName.name,
          active: false,
        })
      : (newTable[columnIndex][rowindex] = {
          name: itemName.name,
          active: true,
        });
    setTable(newTable);
  };

  const addToRefs = (el) => {
    if (el && !blockRefs.current.includes(el)) {
      blockRefs.current.push(el);
    }
  };

  const addToColumnRefs = (el) => {
    if (el && !columnRefs.current.includes(el)) {
      columnRefs.current.push(el);
    }
  };

  const showBlocks = table.map((item, columnIndex) => (
    <div
      className={columnIndex + " column"}
      key={columnIndex}
      ref={addToColumnRefs}
    >
      {item.map((item, index) => (
        <div
          className={item.active ? "block blockActive" : "block blockNotActive"}
          key={index}
          id={index}
          ref={addToRefs}
          onClick={() => toggle(item, index, columnIndex)}
        >
          {/* {item.Name} */}
        </div>
      ))}
    </div>
  ));

  const showNames = () => {

    return (
      <>
        <div className="instrument">{table[0][0].name}</div>
        <div className="instrument">{table[0][1].name}</div>
        <div className="instrument">{table[0][2].name}</div>
        <div className="instrument">{table[0][3].name}</div>
      </>
    );
  };


  const updateSpeed = (sliderValue) => {
    setBpm(sliderValue)
  }
  

  return (
    <>
      <h1>Tracker</h1>
      <button onClick={() => barsMultiplier()}>test</button>
      
    
     
      <div className="drumpanel">
        <div className="instrumentlist">{showNames()}</div>
        <div className="container">{showBlocks}</div>
      </div>
      
      <input type="range" min="10" max="500" value={bpm} onInput={(evt) => updateSpeed(evt.target.value)} />
       BPM: {bpm}
      
    </>
  );
};
