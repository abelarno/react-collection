import { useState, useEffect } from "react";


const cards = [
  {
    card: "Blue",
  },
  {
    card: "Red",
  },
  {
    card: "Green",
  },
  {
    card: "Yellow",
  },
  {
    card: "Purple",
  },
];

function shuffle() {
  const newdeck = cards.concat(cards);

  let currentIndex = newdeck.length,
    randomIndex;
  while (currentIndex > 0) {
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex--;
    [newdeck[currentIndex], newdeck[randomIndex]] = [
      newdeck[randomIndex],
      newdeck[currentIndex],
    ];
  }

  console.log("shuffled");
  return newdeck;
}

const shuffleddeck = shuffle();

export const Memory = () => {
  const [deck, setDeck] = useState(shuffleddeck);
  const [cardsOpen, setCardsOpen] = useState([]);
  const [found, setFound] = useState([]);

  useEffect(() => {
    if (cardsOpen.length == 2) {
      if (deck[cardsOpen[0]].card === deck[cardsOpen[1]].card) {
        setFound([...found, deck[cardsOpen[0]].card]);
        setCardsOpen([]);
        return;
      }

      setTimeout(() => {
        setCardsOpen([]);
      }, "2000");
    }
  }, [cardsOpen]);

  useEffect(() => {
    if (found.length === deck.length / 2) {
      alert("you win");
    }
  }, [found]);

  const renderCards = deck.map((item, index) => {
    const completed = found.includes(item.card);
    const flipped = cardsOpen.includes(index);

    return (
      <div
        className="memorycard"
        key={index}
        onClick={() => {
          if (cardsOpen.length == 2) {
          } else {
            if (!(flipped || completed)) {
              setCardsOpen([...cardsOpen, index]);
            }
          }
        }}
      >
        {completed || flipped ? item.card : "guess me"}
      </div>
    );
  });

  return (
    <>
      <p>Memory </p>
      <div className="cardstack">{renderCards}</div>
      <button
        onClick={() => {
          setDeck(shuffle());
          setCardsOpen([]);
          setFound([]);
        }}
      >
        New game
      </button>

  

    </>
  );
};
