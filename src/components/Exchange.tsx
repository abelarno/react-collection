import { useState } from "react";

export const Exchange = () => {
  const [rates, setRates] = useState({
    AED: 3.847517,
    AFN: 82.195955,
    ALL: 106.503631,
    AMD: 411.745741,
    ANG: 1.9001,
    AOA: 868.394474,
    ARS: 366.630492,
    AUD: 1.657863,
    AWG: 1.885537,
    AZN: 1.778068,
    BAM: 1.956474,
    BBD: 2.130814,
    BDT: 116.240035,
    BGN: 1.955096,
    BHD: 0.394955,
    BIF: 2995.245822,
    BMD: 1.047521,
    BND: 1.446357,
    BOB: 7.285509,
    BRL: 5.304019,
    BSD: 1.054223,
    BTC: 0.000037984519,
    BTN: 87.703043,
    BWP: 14.511582,
    BYN: 3.470395,
    BYR: 20531.407292,
    BZD: 2.125101,
    CAD: 1.435501,
    CDF: 2592.614102,
    CHF: 0.963316,
    CLF: 0.034493,
    CLP: 951.75695,
    CNY: 7.550558,
    COP: 4359.781487,
    CRC: 563.3779,
    CUC: 1.047521,
    CUP: 27.759301,
    CVE: 110.272451,
    CZK: 24.481192,
    DJF: 187.717484,
    DKK: 7.458652,
    DOP: 59.988943,
    DZD: 144.372405,
    EGP: 32.374365,
    ERN: 15.712812,
    ETB: 58.749752,
    EUR: 1,
    FJD: 2.391123,
    FKP: 0.863722,
    GBP: 0.867623,
    GEL: 2.802097,
    GGP: 0.863722,
    GHS: 12.145994,
    GIP: 0.863722,
    GMD: 68.611137,
    GNF: 9051.858345,
    GTQ: 8.286933,
    GYD: 220.575969,
    HKD: 8.202978,
    HNL: 26.001211,
    HRK: 7.348053,
    HTG: 142.8517,
    HUF: 386.880479,
    IDR: 16328.753922,
    ILS: 4.027372,
    IMP: 0.863722,
    INR: 87.146344,
    IQD: 1380.775558,
    IRR: 44257.752675,
    ISK: 146.694616,
    JEP: 0.863722,
    JMD: 163.171525,
    JOD: 0.743113,
    JPY: 156.876517,
    KES: 155.504762,
    KGS: 92.925565,
    KHR: 4355.271341,
    KMF: 491.399971,
    KPW: 942.745144,
    KRW: 1424.869312,
    KWD: 0.323988,
    KYD: 0.878603,
    KZT: 503.083269,
    LAK: 21495.403305,
    LBP: 15846.357698,
    LKR: 342.137837,
    LRD: 195.624595,
    LSL: 19.81908,
    LTL: 3.093057,
    LVL: 0.633635,
    LYD: 5.153578,
    MAD: 10.855739,
    MDL: 19.230623,
    MGA: 4789.512405,
    MKD: 61.569716,
    MMK: 2213.977982,
    MNT: 3639.45085,
    MOP: 8.50633,
    MRO: 373.964738,
    MUR: 46.662439,
    MVR: 16.079321,
    MWK: 1139.759907,
    MXN: 18.548178,
    MYR: 4.948505,
    MZN: 66.255637,
    NAD: 20.109992,
    NGN: 803.416768,
    NIO: 38.57218,
    NOK: 11.402007,
    NPR: 140.319309,
    NZD: 1.772059,
    OMR: 0.403288,
    PAB: 1.054363,
    PEN: 3.995576,
    PGK: 3.865221,
    PHP: 59.541505,
    PKR: 302.447405,
    PLN: 4.61857,
    PYG: 7694.356227,
    QAR: 3.814039,
    RON: 4.973731,
    RSD: 117.269755,
    RUB: 104.270386,
    RWF: 1284.605646,
    SAR: 3.929147,
    SBD: 8.79652,
    SCR: 14.992086,
    SDG: 629.548271,
    SEK: 11.594117,
    SGD: 1.439137,
    SHP: 1.274571,
    SLE: 23.793195,
    SLL: 20688.535255,
    SOS: 596.565597,
    SSP: 628.512289,
    SRD: 40.024746,
    STD: 21681.565214,
    SYP: 13619.511119,
    SZL: 20.126355,
    THB: 38.898679,
    TJS: 11.581589,
    TMT: 3.666323,
    TND: 3.331215,
    TOP: 2.505932,
    TRY: 28.806963,
    TTD: 7.155464,
    TWD: 33.871902,
    TZS: 2629.27734,
    UAH: 38.724628,
    UGX: 3935.909672,
    USD: 1.047521,
    UYU: 40.554355,
    UZS: 12862.429993,
    VEF: 3595491.740198,
    VES: 36.060945,
    VND: 25543.794225,
    VUV: 127.556421,
    WST: 2.913008,
    XAF: 656.1642,
    XAG: 0.049861,
    XAU: 0.000575,
    XCD: 2.830977,
    XDR: 0.801753,
    XOF: 656.157934,
    XPF: 119.574552,
    YER: 262.24659,
    ZAR: 20.196518,
    ZMK: 9428.948299,
    ZMW: 22.166317,
    ZWL: 337.301264,
  });

  const [selectedRate, setSelectedRate] = useState(rates.EUR);

  const [input, setInput] = useState(1);
  const output = input * selectedRate;

  const displayInput = Math.round(input * 100) / 100;
  const displayOutput = Math.round(output * 100) / 100;

  const endpoint = "latest";
  const access_key = "5d9d093cf559061ad3a73ab53463c1c2";

  async function refreshData() {
    const response = await fetch(
      "http://api.exchangeratesapi.io/v1/" +
        endpoint +
        "?access_key=" +
        access_key
    );
    const output = await response.json();
    console.log(output);
    if (output.error !== undefined) {
      alert(output.error.code);
    } else {
      setRates(output.rates);
      console.log(output.rates);
    }
  }

  const showDropdown = Object.entries(rates).map(([key, value]) => (
    <option key={key}>{key}</option>
  ));

  return (
    <>
      <h1>Exchange rates</h1>
      <button onClick={() => refreshData()}>Refresh data</button>
      <div className="sidebyside">
        <div className="leftside">
          <form>
            <label>
              <h2>Euro</h2>
              <input
                type="number"
                className="numberinput"
                id="inputfield"
                value={displayInput}
                onChange={(evt) => {
                  setInput(evt.target.value);
                }}
              />
            </label>
          </form>
        </div>
        <div className="middle">
        
<span className="arrow">></span>
        </div>
        <div className="rightside"><h2>
        <select onChange={(evt) => setSelectedRate(rates[evt.target.value])}>
            <option key="1">Maak een keuze</option>
            {showDropdown}
          </select></h2>
          <input
            className="numberinput"
            type="number"
            value={displayOutput}
            onChange={(evt) => {
              setInput(evt.target.value / selectedRate);
            }}
          />
        </div>
      </div>
    </>
  );
};
